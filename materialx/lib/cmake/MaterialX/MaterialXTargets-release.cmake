#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "MaterialXCore" for configuration "Release"
set_property(TARGET MaterialXCore APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXCore PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXCore.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXCore.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXCore )
list(APPEND _cmake_import_check_files_for_MaterialXCore "${_IMPORT_PREFIX}/lib/libMaterialXCore.dylib" )

# Import target "MaterialXFormat" for configuration "Release"
set_property(TARGET MaterialXFormat APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXFormat PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXFormat.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXFormat.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXFormat )
list(APPEND _cmake_import_check_files_for_MaterialXFormat "${_IMPORT_PREFIX}/lib/libMaterialXFormat.dylib" )

# Import target "MaterialXGenShader" for configuration "Release"
set_property(TARGET MaterialXGenShader APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXGenShader PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXGenShader.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXGenShader.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXGenShader )
list(APPEND _cmake_import_check_files_for_MaterialXGenShader "${_IMPORT_PREFIX}/lib/libMaterialXGenShader.dylib" )

# Import target "MaterialXGenGlsl" for configuration "Release"
set_property(TARGET MaterialXGenGlsl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXGenGlsl PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXGenGlsl.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXGenGlsl.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXGenGlsl )
list(APPEND _cmake_import_check_files_for_MaterialXGenGlsl "${_IMPORT_PREFIX}/lib/libMaterialXGenGlsl.dylib" )

# Import target "MaterialXGenOsl" for configuration "Release"
set_property(TARGET MaterialXGenOsl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXGenOsl PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXGenOsl.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXGenOsl.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXGenOsl )
list(APPEND _cmake_import_check_files_for_MaterialXGenOsl "${_IMPORT_PREFIX}/lib/libMaterialXGenOsl.dylib" )

# Import target "MaterialXGenMdl" for configuration "Release"
set_property(TARGET MaterialXGenMdl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXGenMdl PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXGenMdl.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXGenMdl.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXGenMdl )
list(APPEND _cmake_import_check_files_for_MaterialXGenMdl "${_IMPORT_PREFIX}/lib/libMaterialXGenMdl.dylib" )

# Import target "MaterialXGenMsl" for configuration "Release"
set_property(TARGET MaterialXGenMsl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXGenMsl PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXGenMsl.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXGenMsl.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXGenMsl )
list(APPEND _cmake_import_check_files_for_MaterialXGenMsl "${_IMPORT_PREFIX}/lib/libMaterialXGenMsl.dylib" )

# Import target "MaterialXRender" for configuration "Release"
set_property(TARGET MaterialXRender APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXRender PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXRender.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXRender.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXRender )
list(APPEND _cmake_import_check_files_for_MaterialXRender "${_IMPORT_PREFIX}/lib/libMaterialXRender.dylib" )

# Import target "MaterialXRenderGlsl" for configuration "Release"
set_property(TARGET MaterialXRenderGlsl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXRenderGlsl PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXRenderGlsl.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXRenderGlsl.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXRenderGlsl )
list(APPEND _cmake_import_check_files_for_MaterialXRenderGlsl "${_IMPORT_PREFIX}/lib/libMaterialXRenderGlsl.dylib" )

# Import target "MaterialXRenderMsl" for configuration "Release"
set_property(TARGET MaterialXRenderMsl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXRenderMsl PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXRenderMsl.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXRenderMsl.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXRenderMsl )
list(APPEND _cmake_import_check_files_for_MaterialXRenderMsl "${_IMPORT_PREFIX}/lib/libMaterialXRenderMsl.dylib" )

# Import target "MaterialXRenderHw" for configuration "Release"
set_property(TARGET MaterialXRenderHw APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXRenderHw PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXRenderHw.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXRenderHw.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXRenderHw )
list(APPEND _cmake_import_check_files_for_MaterialXRenderHw "${_IMPORT_PREFIX}/lib/libMaterialXRenderHw.dylib" )

# Import target "MaterialXRenderOsl" for configuration "Release"
set_property(TARGET MaterialXRenderOsl APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MaterialXRenderOsl PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libMaterialXRenderOsl.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libMaterialXRenderOsl.dylib"
  )

list(APPEND _cmake_import_check_targets MaterialXRenderOsl )
list(APPEND _cmake_import_check_files_for_MaterialXRenderOsl "${_IMPORT_PREFIX}/lib/libMaterialXRenderOsl.dylib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
